package creations.nag.tagalogdictionary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import creations.nag.tagalogdictionary.dictionary.Dictionary;
import creations.nag.tagalogdictionary.dictionary.DictionarySearch;

public class MainActivity extends AppCompatActivity {

    DictionarySearch dictionarySearch;
    EditText searchBar;
    Button searchButton;
    Button clearButton;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchBar = (EditText) findViewById(R.id.searchBar);
        searchButton = (Button) findViewById(R.id.searchButton);
        clearButton = (Button) findViewById(R.id.clearButton);
        result = (TextView) findViewById(R.id.result);

        try {
            loadDictionary();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dictionarySearch = new DictionarySearch();

        //Makes the app search for the word in the dictionary
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = searchBar.getText().toString();
                if (input.length() < 1) //The search bar is blank
                {
                    Toast.makeText(MainActivity.this, "Enter a word", Toast.LENGTH_SHORT).show(); //Shows if no character is entered
                }
                else
                {
                    try {
                        String res = dictionarySearch.search(input, Dictionary.SINGLETON.getTagalogDictionary(), Dictionary.SINGLETON.getEnglishDictionary());
                        result.setText(res.toString());
                    } catch (Exception e) {}
                }
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public  void onClick(View v) {
                searchBar.getText().clear();
            }
        });

    }

    /*
     * This method gives the MainActivity context to the Dictionary class.
     * Then it initializes the dictionary on runtime with all the Tagalog and English words
     */
    private void loadDictionary() throws IOException
    {
        try {
            Dictionary.SINGLETON.setContext(this); //Set's dictionary's context to MainActivity's context
            Dictionary.SINGLETON.initializeTagDictionary();
            Dictionary.SINGLETON.initializeEngDictionary();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}