package creations.nag.tagalogdictionary.dictionary;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/*
 * This class will hold all the words in the dictionary.txt file
 * Only one instance of Dictionary will be created. Thus, the use of the Singleton method.
 */
public class Dictionary {

    public static Dictionary SINGLETON = new Dictionary();

    /*
     * This map will contain all the words in the dictionary.
     * @key String a Tagalog word.
     * @value String an English word.
     */
    private static Map<String, String> TagalogDictionary;
    private static Map<String, String> EnglishDictionary;
    private Context context;

    private Dictionary() {
        TagalogDictionary = new HashMap<>();
        EnglishDictionary = new HashMap<>();
    }

    public void initializeTagDictionary() throws IOException {
        try {

            AssetManager am = context.getAssets(); //Grabs everything in the /scr/main/assets directory
            InputStream Tagalog_To_English = am.open("Tagalog_To_English.txt");

            BufferedReader brTagalog_To_English = new BufferedReader(new InputStreamReader(Tagalog_To_English)); //Holds all the words in the dictionary
            String tagalog = null;

            //Ensures all the Tagalog words get read in
            while ((tagalog = brTagalog_To_English.readLine()) != null) {
                String[] TagalogSplit = tagalog.split(": "); //[0] is the tagalog, [1] is the english
                TagalogDictionary.put(TagalogSplit[0], TagalogSplit[1]); //Tagalog word is a key, English word is the value.
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializeEngDictionary() {
        try {
            AssetManager am = context.getAssets(); //Grabs everything in the /scr/main/assets directory
            InputStream English_To_Tagalog = am.open("English_To_Tagalog.txt");

            BufferedReader brEnglish_To_Tagalog = new BufferedReader(new InputStreamReader(English_To_Tagalog)); //Holds all the meanings in the dictionary.
            String english = null;

            //Ensures all the English words get read in
            while ((english = brEnglish_To_Tagalog.readLine()) != null)
            {
                String[] EnglishSplit = english.split(": "); //[0] is the english, [1] is the tagalog
                EnglishDictionary.put(EnglishSplit[0], EnglishSplit[1]); //English word is a key, Tagalog word is the value.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Getters
    public Map getTagalogDictionary() { return TagalogDictionary; }

    public Map getEnglishDictionary() { return EnglishDictionary; }

    //Setters
    public void setContext(Context context) {
        this.context = context;
    }
}

