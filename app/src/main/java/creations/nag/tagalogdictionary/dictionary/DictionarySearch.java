package creations.nag.tagalogdictionary.dictionary;

import java.util.Map;

/**
 * Created by natha on 1/27/2017.
 */

public class DictionarySearch {


    //Constructor
    public DictionarySearch() {}

    //Functions
    public String search(String search, Map<String, String> tagalogDictionary, Map<String, String> englishDictionary)
    {
        search = search.toLowerCase(); //Ignore letter case for ease
        search = search.trim(); //Gets rid of any extra whitespace
        String definition = "Word doesn't exist in dictionary"; //default case

        if (tagalogDictionary.containsKey(search))
        {
            definition = SearchTagalog(search, tagalogDictionary);
        }
        else if (englishDictionary.containsKey(search))
        {
            definition = SearchEnglish(search, englishDictionary);
        }

        return definition;
    }

    /*
     * Helper method for the search method.
     * Strictly searches the Tagalog dictionary.
     * Updates the data member definition as well.
     */
    public String SearchTagalog(String search, Map<String, String> tagalogDictionary)
    {
        String output = "Word doesn't exist in dictionary";
        try {
            for (Map.Entry<String, String> map : tagalogDictionary.entrySet())
            {
                boolean isRepeat = false; //Checks if there's a duplicate word in the dictionary
                if (map.getKey().equals(search) && !isRepeat)
                {
                    output = map.getValue();
                }
                else if (map.getKey().equals(search) && isRepeat) //If there's a duplicate, show it's other meaning too
                {
                    output = "; " + map.getValue();
                }
            }
            return output;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    /*
     *  Helper method for the search method.
     * Strictly searches the Tagalog dictionary.
     * Updates the data member definition as well.
     */
    public String SearchEnglish(String search, Map<String, String> englishDictionary)
    {
        String output = "Word does not exist in the dictionary!";
        try {
            for (Map.Entry<String, String> map : englishDictionary.entrySet())
            {
                boolean isRepeat = false; //Checks if there's a duplicate word in the dictionary
                if (map.getKey().equals(search) && !isRepeat)
                {
                    output = map.getValue();
                }
                else if (map.getKey().equals(search) && isRepeat) //If there's a duplicate, show it's other meaning too
                {
                    output = "; " + map.getValue();
                }
            }
            return output;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
}
