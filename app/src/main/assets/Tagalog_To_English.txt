aba: well!; hey!
abakada: alphabet
abala: delay; trouble, bother, inconvenience; busy, occupied
umabala: to delay, hinder;
maabala: to be inconvenienced
abang: watcher
mag-abang: to watch, wait for something
abat: ambush; person lying in wait
abatan: to ambush: intercept: keep watch over
abilidad: ability
abo: ash
abogado: lawyer
abono: reimbursement
abong: dense blown smoke or dust
abot: reach; range
abutan: to hand something over
iabot: to handsomething over
umabot: to reach for
abutin: to reach for something
abu-abo: mist, haze
abubot: knicknacks, trinkets
abuso: abuse
akalain: to think, assume; suppose, presume
akay: leading, guiding
umakay: to lead, guide
akibat: carrying over the shoulder and across the body
nakaakibat: wearing something over the shoulder and across the body
akin: my, mine: belonging to me
akit: attract, charm
akitin: to attract, charm
kaakit-akit: attractive, charming, fascinating
aklat: book
aklatan: library
akma: proper, suited, fitting; threatening gesture
iakma: to fit, adjust
pagkaakma: agreement, accordance
umakma: to threaten to do; be on the verge of doing
ako: me, I
makaako: selfish
pangako: promise
ipangako: to promise
akyat: climb, rise
umakyat: to ascend, climb
akyat-bahay: burglar (slang)
adhika: amin, intention, objective, goal; amibition
adhikain: to strive to attain or accomplish
aga: earliness
maaga: early
kinamagahan: the next morning
agahan: breakfast
umaga: morning
agad: immediately, at once
kaagad: immediately, at once
agapay: side by side; escort;
agaw: snatching, grabbing
agawin: to snatch, grab
agham: science
agila: eagle
agimat: amulet
agnas: erosion
umagnas: to erode
agos: current of water; flow
umagos: to flow
agusan: to flow
agwas: mullet fish
agwat: gap, distance
agwat-agwat: with gaps in between
ahas: snake
ahit: see "mag-ahit"
mag-ahit: to shave
umahon: to go up
alaala: memory
maalaala: to remember
magpaalaala: to remind
magpaalaala: to remind about something
alak: wine, alcohol
alagaan: to attend to someone, care for someone
mag-alaga: to care for
alagad: follower; disciple
alahas: jewels, jewelery (accessory)
alahasan: jewelery store
alala: worried, anxious
mag-alala: to worry
alalahanin: to remember, recall; cause of worry
alam: knowledge
alam: to know
alamin: to investigate; find out
kaalaman: knowledge
kinalaman: knowledge of an event or deed
ipaalam: to inform; enlighten
makaalam: to know
makialam: to meddle, be in someone's business
malaman: to know; learn
may kinalaman: concern, relate to, have to do with
alamang: species ofsmall shrimp
alamat: legend; myth
alang: see "alang-alang"
alang-alang: in regard, benefit of
alat: saltiness
maalat: salty
alawak: bubbling gush, sudden gush
alawab: slime
alawas: derailed; out of alignment; long-handed fishing net
alay: offering
ialay: to offer something
mag-alay: to offer
albakora: yellowfin tuna, albacore
albay: support at the side of a house
ale: aunt; term of respect for a woman one generation older than the speaker
aleman: German
alibugha: wasteful; irresponsible
alikabok: dust
alikmata: confused state; loss of one's sense of direction; pupil of the eye
alikot: laziness; following around idly
aliktiya: offensive words; insult
aligata: concern; considerateness
aligutgot: entangled; trouble; mischief
alimango: species of crab
alimasag: species of crab smaller than the "alimango"
alinlangan: doubt
alimbukay: nausea; swell of water
alimbuyugin: red-colored rooster with black spots on the wings
alimis: secret, furtive
alimpapayaw: gliding flight
alimpungat: half-awake
alimpuso: knot of wood
alimpuyo: whirl, eddy
alimpuyok: strong emission of steam or smoke
alimulon: cone shaped
aretes: earing
bakla: gay (homosexual)
bahag: loincloth (for males)
balingkinitan: slender
bangka: small boat
baon: packed food; bury
baro: jacket for women (traditional)
batalan: deck with no roof (outside of house)
bukang-liwayway: dawn (time of day)
busio: storage room
magbaon: to pack food
ibaon: to bury something
kabaong: coffin
kabihasnan: civilization
kalumbiga: armlets, bracelets
kanggan: sleeveless shirt
kasulukuyan: current, present time
kayumanggi: brown (color)
kingki: curly (hair)
kutya: scoff, sneer
digma: war
dyolens: marble
galang: respect
galang: anklet
gumon: addicted
nakakagumon: addictive
nakakapagpabagabag: bothersome
hikaw: earing
lagalag: nomad
laot: midst
lason: poison
linggatong: perplexity
mahusay: good
mangaso: to hunt
mapadpad: to happen to end up somewhere (location)
palamuti: decoration, ornament
palawit: pendant
pana: bow and arrow
patadyong: loose skirt
pinakamakapangyarihan: omniscient, almighty
pukol: thrown
pustahan: to bet
ipukol: to throw
putong: warrior hat for males
salakay: attack
salamat: thank you
sumalakay: to attack
salakayin: to attack something/someone
sandata: weapon
saya: long dress/skirt for women
sibat: spear
silong: covering (of a window)
silong: top of the bottom/under of a nipa hut
singing: ring
sumpit: blow gun (dart)
tuwid: straight
yungnib: cave












